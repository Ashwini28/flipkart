package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//button[@class ='_2AkmmA _29YdH8']") WebElement eleclose;
	@FindBy(how = How.XPATH, using = "//span[text()='Electronics']") WebElement eleselect;
	@FindBy(how = How.LINK_TEXT, using = "Realme") WebElement elerealme;
	
	
	public HomePage clickClose()
	{
		eleclose.click();
		return this;
	}
	public MasterPage clickonMenu() {
		
		mouseAction(eleselect, elerealme);
		
		return new MasterPage();
	}
	
	
}
