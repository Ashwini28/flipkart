package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import wdMethods.ProjectMethods;

public class MasterPage extends ProjectMethods {



	public MasterPage()
	{
		PageFactory.initElements(driver, this);


	}
	@FindBy(how = How.XPATH, using ="(//div[text()='Realme'])[1]") WebElement eletitle;
	@FindBy(how = How.XPATH, using ="//div[text()='Newest First']") WebElement elenewclick;
	@FindBy(how = How.XPATH, using ="//div[@class='_3wU53n']") List<WebElement> elebrand;
	@FindBy(how = How.XPATH, using ="//div[@class='_1vC4OE _2rQ-NK']") List<WebElement> eleprice;
	@FindBy(how = How.XPATH, using ="(//div[@class='_3wU53n'])[1]") WebElement eletext;
	@FindBy(how = How.XPATH, using = "(//div[@class='_3wU53n'])[1]") WebElement elefirstresult;



	public MasterPage verifyTitle() throws InterruptedException {
		Thread.sleep(2000);
		verifyPartialText(eletitle, "Realme");
		return this;
	}

	public MasterPage clickNewestFirst() throws InterruptedException
	{
		waitForElement(elenewclick);
		Thread.sleep(2000);
		return this;
	}

	public MasterPage getBrandName() {
		for (int i = 0; i < elebrand.size(); i++) {
			System.out.println("Brand Name: "+elebrand.get(i).getText()+" , "+"Price: "+eleprice.get(i).getText().replaceAll("\\D", ""));
		}
		return this;
	}	

	public MasterPage firstText()
	{
		text = eletext.getText();
		return this;
	}

	public NewResult firstresultclicked()
	{
		elefirstresult.click();
		switchToWindow(1);
		return new NewResult();
	}


}