package pages;

import java.awt.Window;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import wdMethods.ProjectMethods;

public class NewResult extends ProjectMethods {
	
	

	public NewResult()
	{
		PageFactory.initElements(driver, this);

	}
	
	@FindBy(how = How.XPATH, using ="(//span[@class='_38sUEc'])[1]") WebElement elereview;
	
	public NewResult titleVerify() {
		verifyTitle(text);
		return this;
	}
	
	public NewResult reviewandrating()
	{
		
		System.out.println(elereview.getText());
		return this;
	}
	



}

