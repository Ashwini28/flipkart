package testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class FlipkartLaunch {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElementByXPath("//button[@class ='_2AkmmA _29YdH8']").click();
		WebElement eleElectronics = driver.findElementByXPath("//span[text()='Electronics']");
		Actions action = new Actions(driver);
		action.moveToElement(eleElectronics).perform();
		driver.findElementByLinkText("Realme").click();
		WebElement eletitle = driver.findElementByXPath("(//div[text()='Realme'])[1]");
		String text1 = eletitle.getText();
		if (text1.contains("Realme"))
			System.out.println("The tile is verified as Realme");
		else
			System.out.println("The title is not Realme");
		driver.findElementByXPath("//div[text()='Newest First']").click();
		Thread.sleep(3000);

		List<WebElement> brandNames = driver.findElementsByXPath("//div[@class='_3wU53n']");
		for (WebElement eachbrand : brandNames) {
			System.out.println(eachbrand.getText());
		}

		List<WebElement> prices = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
		for (WebElement eachprice : prices) {
			
			System.out.println(eachprice.getText().replaceAll("\\D", ""));

		}
		String text = driver.findElementByXPath("(//div[@class='_3wU53n'])[1]").getText();
		driver.findElementByXPath("(//div[@class='_3wU53n'])[1]").click();
		
		Set<String> window1 = driver.getWindowHandles();
		List<String> list = new ArrayList<>();
		list.addAll(window1);
		driver.switchTo().window(list.get(1));
		String title1 = driver.getTitle();
		
		System.out.println(title1);
		if(title1.contains(text))
			System.out.println("The title is window 2 is verified as Realme");
		else
			System.out.println("The tile in window 2 is not verified");
		
		WebElement elereview = driver.findElementByXPath("(//span[@class='_38sUEc'])[1]");
		System.out.println(elereview.getText());
		
	}
	

}
