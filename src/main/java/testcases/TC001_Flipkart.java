package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.HomePage;
import wdMethods.ProjectMethods;


public class TC001_Flipkart extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_Flipkart";
		testDescription = "Flipkart Launch";
		authors = "Ashwini";
		category = "smoke";
		dataSheetName = "TC001";
		testNodes = "Flipkart";
	}
	
	@Test
	public void launch() throws InterruptedException {		
		new HomePage()
		.clickClose()
		.clickonMenu()
		.verifyTitle()
		.clickNewestFirst()
		.getBrandName()
		.firstText()
		.firstresultclicked()
		.titleVerify()
		.reviewandrating();
		
	}
	
}


